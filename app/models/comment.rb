class Comment < ActiveRecord::Base
  belongs_to :post
  
  validates_presence_of :post_id, :only_integer => true, :message => "must reference the ID expressed as an integral" 
  #post_id must be an integer, this is not human friendly
  
  validates_presence_of :body
end
